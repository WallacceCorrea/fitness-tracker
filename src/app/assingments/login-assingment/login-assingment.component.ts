import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login-assingment',
  templateUrl: './login-assingment.component.html',
  styleUrls: ['./login-assingment.component.css']
})
export class LoginAssingmentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

}
