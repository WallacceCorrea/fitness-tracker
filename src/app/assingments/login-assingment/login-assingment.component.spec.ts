import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAssingmentComponent } from './login-assingment.component';

describe('LoginAssingmentComponent', () => {
  let component: LoginAssingmentComponent;
  let fixture: ComponentFixture<LoginAssingmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginAssingmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAssingmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
